import React, {useEffect, useState} from 'react';
import {RefreshControl} from 'react-native';

import {useApiState} from '../../contexts/ApiContext';

import {useIsFocused} from '@react-navigation/native';
import {Modal} from 'react-native-paper';

import Screen from '../../components/Screen';
import Header from '../../components/Header';

import {Props} from '../types';

import styles from './styles';
import Filters from '../../components/Filters';
import List from '../../components/List';

export default ({navigation}: Props): JSX.Element => {
  const apiState = useApiState();
  const isFocused = useIsFocused();

  const [searchString, setSearchString] = useState<string>('');
  const [languageSearchString, setLanguageSearchString] = useState<string>('');
  const [topicSearchString, setTopicSearchString] = useState<string>('');
  const [usernameSearchString, setUsernameSearchString] = useState<string>('');
  const [selectedSort, setSelectedSort] = useState<string | null>(null);
  const [toggleSearch, setToggleSearch] = useState<boolean>(true);
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const [currentPage, setCurrentPage] = useState<number>(1);

  // Get /search/repositories?...
  const fetchRepos = async (): Promise<void> => {
    await apiState.actions.fetchRepos({
      q: searchString.concat(
        languageSearchString !== '' ? `+language:${languageSearchString}` : '',
        topicSearchString !== '' ? `+topic:${topicSearchString}` : '',
        usernameSearchString !== '' ? `+user:${setUsernameSearchString}` : '',
      ),
      sort:
        selectedSort != null && selectedSort !== '' ? selectedSort : 'stars',
      page: currentPage,
      order: 'desc',
      per_page: 10,
    });
  };

  // Fetch Data every 60 seconds
  useEffect(() => {
    const interval: NodeJS.Timeout = setInterval(() => {
      fetchRepos();
    }, 60000);

    return () => {
      clearInterval(interval);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused]);

  // Fetch data whenever search string or filters change
  useEffect(() => {
    fetchRepos();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [toggleSearch, currentPage]);

  // Triggers new api request
  const _toggleSearch = (): void => {
    setShowFilters(false);
    setToggleSearch(!toggleSearch);
  };

  // Handler methods
  const _changeSearchText = (value: string): void => setSearchString(value);
  const _changeLanguageSearchText = (value: string): void =>
    setLanguageSearchString(value);
  const _changeTopicSearchText = (value: string): void =>
    setTopicSearchString(value);
  const _changeUsernameSearchText = (value: string): void =>
    setUsernameSearchString(value);
  const _changePicker = (itemValue: string): void => setSelectedSort(itemValue);
  const _increasePage = (): void => setCurrentPage(currentPage + 1);
  const _decreasePage = (): void => setCurrentPage(currentPage - 1);
  const _toggleFilters = (): void => setShowFilters(!showFilters);
  const _hideFilters = (): void => setShowFilters(false);

  return (
    <Screen
      header={
        <Header
          title={'GitHub Repos'}
          searchString={searchString}
          changeSearchText={_changeSearchText}
          toggleSearch={_toggleSearch}
          toggleFilters={_toggleFilters}
        />
      }
      scrollable
      refreshControl={
        <RefreshControl
          refreshing={apiState.values.refreshing}
          onRefresh={fetchRepos}
        />
      }
      customStyles={styles.container}>
      <List
        navigation={navigation}
        currentPage={currentPage}
        increasePage={_increasePage}
        decreasePage={_decreasePage}
      />
      <Modal visible={showFilters} onDismiss={_hideFilters}>
        <Filters
          toggleSearch={_toggleSearch}
          languageSearchString={languageSearchString}
          changeLanguageSearchText={_changeLanguageSearchText}
          topicSearchString={topicSearchString}
          changeTopicSearchText={_changeTopicSearchText}
          usernameSearchString={usernameSearchString}
          changeUsernameSearchText={_changeUsernameSearchText}
          changePicker={_changePicker}
        />
      </Modal>
    </Screen>
  );
};
