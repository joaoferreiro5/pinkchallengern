import React, {useEffect} from 'react';
import {RefreshControl} from 'react-native';

import {useStorageState} from '../../contexts/StorageContext';
import {useIsFocused} from '@react-navigation/native';

import Screen from '../../components/Screen';
import Header from '../../components/Header';

import {Props} from '../types';

import styles from './styles';
import OfflineList from '../../components/OfflineList';

export default ({navigation}: Props): JSX.Element => {
  const storageState = useStorageState();
  const isFocused = useIsFocused();

  const fetchRepos = async (): Promise<void> =>
    await storageState.actions.getRepos();

  useEffect(() => {
    fetchRepos();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused]);

  return (
    <Screen
      header={<Header title={'Saved Repos'} offline />}
      scrollable
      refreshControl={
        <RefreshControl
          refreshing={storageState.values.loading}
          onRefresh={fetchRepos}
        />
      }
      customStyles={styles.container}>
      <OfflineList navigation={navigation} />
    </Screen>
  );
};
