import {StyleSheet} from 'react-native';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 24,
  },
  item: {
    width: '90%',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingBottom: 8,
    borderBottomWidth: 1,
    borderRightWidth: 1,
    marginTop: 24,
    marginHorizontal: 24,
  },
  title: {
    width: '35%',
    fontSize: 14,
    lineHeight: 16,
    color: Colors.darkgray,
  },
  description: {
    maxWidth: '65%',
    fontSize: 14,
    lineHeight: 16,
    color: Colors.black,
  },
  url: {
    textDecorationLine: 'underline',
    color: Colors.blue,
  },
  topicDescription: {
    maxWidth: '80%',
    fontSize: 14,
    lineHeight: 16,
    color: Colors.black,
  },
  button: {
    alignSelf: 'center',
    marginVertical: 24,
  },
});
