import React, {useEffect} from 'react';
import {View, Text, Linking, RefreshControl} from 'react-native';

import {useStorageState} from '../../contexts/StorageContext';

import dayjs from 'dayjs';

import Screen from '../../components/Screen';
import Header from '../../components/Header';

import styles from './styles';
import {Props} from '../types';
import {Button} from 'react-native-paper';

export default ({navigation, route}: Props): JSX.Element => {
  const storageState = useStorageState();

  const [existsInStorage, setExistsInStorage] = React.useState<boolean>(false);

  const _goBack = (): void => navigation.goBack();
  const _handleUrl = async (url: string): Promise<void> =>
    (await Linking.canOpenURL(url)) ? Linking.openURL(url) : null;

  const checkItem = async (): Promise<void> => {
    const doesItemExist = await storageState.actions.doesItemExist(
      route?.params?.item.id ?? -1,
    );
    if (doesItemExist) {
      setExistsInStorage(true);
    } else {
      setExistsInStorage(false);
    }
  };

  useEffect(() => {
    checkItem();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderArray = [
    {
      title: 'Project Name:',
      description: route?.params?.item.name ?? '-',
    },
    {
      title: 'Description:',
      description: route?.params?.item.description ?? '-',
    },
    {
      render: (
        <View key={'popularity'} style={styles.item}>
          <Text style={styles.title}>{'Popularity: '}</Text>
          <Text
            style={
              styles.description
            }>{`Watchers: ${route?.params?.item.watchers_count}\nStars: ${route?.params?.item.stargazers_count}\nForks: ${route?.params?.item.forks_count}\nOpen Issues: ${route?.params?.item.open_issues_count}`}</Text>
        </View>
      ),
    },
    {
      title: 'License:',
      description: route?.params?.item.license?.name ?? '-',
    },
    {
      render: (
        <View key={'url'} style={styles.item}>
          <Text style={styles.title}>{'Url: '}</Text>
          <Text
            testID="url"
            onPress={(): Promise<void> =>
              _handleUrl(route?.params?.item.html_url ?? '')
            }
            style={[
              styles.description,
              route?.params?.item.html_url !== '' && styles.url,
            ]}>
            {route?.params?.item.html_url !== ''
              ? route?.params?.item.html_url
              : '-'}
          </Text>
        </View>
      ),
    },
    {
      title: 'Created At:',
      description: dayjs(route?.params?.item.created_at).format('LLL') ?? '-',
    },
    {
      title: 'Last Updated:',
      description: dayjs(route?.params?.item.updated_at).format('LLL') ?? '-',
    },
    {
      title: 'Language:',
      description: route?.params?.item.language ?? '-',
    },
    {
      title: 'Owner:',
      description: route?.params?.item.owner.login ?? '-',
    },
    {
      render: (
        <View key={'homepage'} style={styles.item}>
          <Text style={styles.title}>{'Homepage: '}</Text>
          <Text
            testID="url"
            onPress={(): Promise<void> =>
              _handleUrl(route?.params?.item.homepage ?? '')
            }
            style={[
              styles.description,
              route?.params?.item.homepage !== '' && styles.url,
            ]}>
            {route?.params?.item.homepage !== ''
              ? route?.params?.item.homepage
              : '-'}
          </Text>
        </View>
      ),
    },
    {
      title: 'Has Wiki?',
      description: route?.params?.item.has_wiki ? 'Yes' : 'No',
    },
    {
      title: 'Has Pages?',
      description: route?.params?.item.has_pages ? 'Yes' : 'No',
    },
    {
      title: 'Has Issues?',
      description: route?.params?.item.has_issues ? 'Yes' : 'No',
    },
    {
      title: 'Has Projects?',
      description: route?.params?.item.has_projects ? 'Yes' : 'No',
    },
    {
      render: (
        <View key={'topic'} style={styles.item}>
          <Text style={styles.title}>{'Topics: '}</Text>
          <View>
            <Text style={styles.topicDescription}>
              {route?.params?.item.topics.map((item: string) => `${item}, `)}
            </Text>
          </View>
        </View>
      ),
    },
  ];

  const renderItems = (): JSX.Element[] =>
    renderArray.map(
      (
        item: {title?: string; description?: string; render?: JSX.Element},
        index: number,
      ) =>
        item.render != null ? (
          item.render
        ) : (
          <View key={`item-${index}`} style={styles.item}>
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.description}>{item.description}</Text>
          </View>
        ),
    );

  return (
    <Screen
      header={
        <Header
          title={route?.params?.item.name ?? ''}
          goBack={_goBack}
          addItemToStorage={
            !existsInStorage
              ? (): void => {
                  storageState.actions.addRepo(route?.params?.item.id ?? -1);
                  checkItem();
                }
              : undefined
          }
        />
      }
      refreshControl={
        <RefreshControl
          refreshing={storageState.values.loading}
          // onRefresh={}
        />
      }
      scrollable
      customStyles={styles.container}>
      <View style={styles.content}>{renderItems()}</View>
      {existsInStorage ? (
        <Button
          testID="deleteButton"
          style={styles.button}
          mode="contained"
          onPress={(): void => {
            storageState.actions.removeRepo(route?.params?.item.id ?? -1);
            checkItem();
          }}>
          {'Delete item from storage'}
        </Button>
      ) : (
        <></>
      )}
    </Screen>
  );
};
