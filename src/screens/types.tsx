import {NavigationProp, ParamListBase} from '@react-navigation/native';
import {RepoInterface} from '../contexts/types';

export interface Props {
  route?: {
    params?: {
      item: RepoInterface;
    };
  };
  navigation: NavigationProp<ParamListBase>;
}
