import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

import {NavigationProp, ParamListBase} from '@react-navigation/native';

import {useApiState} from '../../contexts/ApiContext';
import {RepoInterface} from '../../contexts/types';

import Icon from 'react-native-vector-icons/MaterialIcons';

import ListItem from '../../components/ListItem';

import styles from './styles';

interface Props {
  navigation: NavigationProp<ParamListBase>;
  currentPage: number;
  increasePage: () => void;
  decreasePage: () => void;
}

export default ({
  navigation,
  currentPage,
  increasePage,
  decreasePage,
}: Props): JSX.Element => {
  const apiState = useApiState();

  return (
    <>
      <View style={styles.container}>
        {apiState.values.repos != null && apiState.values.repos.length !== 0 ? (
          apiState.values.repos.map((item: RepoInterface, index: number) => (
            <ListItem
              navigation={navigation}
              item={item}
              key={`list-item-${index}`}
            />
          ))
        ) : (
          <Text style={styles.emptySearchText}>
            {'Please insert a search query to receive results'}
          </Text>
        )}
      </View>
      {apiState.values.repos != null &&
      apiState.values.totalRepoItems != null &&
      apiState.values.repos.length !== 0 ? (
        <View style={styles.paginationContainer}>
          <Text>
            {`Showing page ${currentPage} of ${Math.ceil(
              apiState.values.totalRepoItems / 10,
            )}`}{' '}
          </Text>
          <TouchableOpacity
            disabled={currentPage === 1}
            onPress={decreasePage}
            style={[
              styles.iconContainer,
              currentPage === 1 && styles.disabledIconContainer,
            ]}>
            <Icon name="chevron-left" size={32} color={'black'} />
          </TouchableOpacity>
          <TouchableOpacity
            disabled={currentPage === apiState.values.totalRepoItems / 10}
            onPress={increasePage}
            style={[
              styles.iconContainer,
              currentPage === apiState.values.totalRepoItems / 10 &&
                styles.disabledIconContainer,
            ]}>
            <Icon name="chevron-right" size={32} color={'black'} />
          </TouchableOpacity>
        </View>
      ) : (
        <></>
      )}
    </>
  );
};
