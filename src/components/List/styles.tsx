import {StyleSheet} from 'react-native';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 80,
    marginTop: 24,
    width: '100%',
  },
  emptySearchText: {
    marginTop: '50%',
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 24,
    lineHeight: 32,
    color: Colors.lightgray,
  },
  paginationContainer: {
    position: 'absolute',
    bottom: 16,
    right: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  iconContainer: {
    padding: 8,
    borderRadius: 8,
    margin: 8,
    backgroundColor: Colors.lightblue,
  },
  disabledIconContainer: {
    backgroundColor: Colors.lightgray,
  },
});
