import React, {useState, useEffect, useRef} from 'react';
import {
  Animated,
  View,
  Text,
  TouchableOpacity,
  Easing,
  Image,
} from 'react-native';

import {NavigationProp, ParamListBase} from '@react-navigation/native';

import Icon from 'react-native-vector-icons/MaterialIcons';

import {RepoInterface} from '../../contexts/types';

import styles from './styles';
import globalStyles from '../../utils/globalStyles';

export interface Props {
  navigation: NavigationProp<ParamListBase>;
  item: RepoInterface;
  offline?: boolean;
}

export default ({navigation, item, offline}: Props): JSX.Element => {
  const [isExpanded, setIsExpanded] = useState<boolean>(false);
  const animationHeight = useRef(new Animated.Value(100)).current;

  useEffect(() => {
    if (isExpanded) {
      Animated.timing(animationHeight, {
        duration: 100,
        toValue: 200,
        easing: Easing.linear,
        useNativeDriver: false,
      }).start();
    } else {
      Animated.timing(animationHeight, {
        duration: 100,
        toValue: 100,
        easing: Easing.linear,
        useNativeDriver: false,
      }).start();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isExpanded]);

  const _navigateTo = (): void =>
    navigation.navigate('Repository', {item: item});

  const _toggleExpansion = (): void => setIsExpanded(!isExpanded);

  return (
    <Animated.View
      testID="parent"
      style={[
        styles.container,
        {height: animationHeight},
        globalStyles.shadow,
      ]}>
      <TouchableOpacity
        style={styles.content}
        testID="navigateIcon"
        onPress={_navigateTo}>
        <Text
          numberOfLines={isExpanded ? undefined : 1}
          style={styles.repoName}>
          {item.name}
        </Text>
        <Text numberOfLines={isExpanded ? 6 : 2} style={styles.repoDescp}>
          {item.description}
        </Text>
        {isExpanded && (
          <View testID="extraInfo" style={styles.extraInfoContainer}>
            <View style={styles.extraInfoItem}>
              <Text style={styles.extraInfoText}>{item.language}</Text>
            </View>
            <View style={styles.extraInfoItem}>
              <Icon name="star-outline" size={24} color="black" />
              <Text style={styles.extraInfoText}>{item.stargazers_count}</Text>
            </View>
            {!offline && (
              <View style={styles.extraInfoItem}>
                <Text style={styles.extraInfoText}>{'Owner: '}</Text>
                <Image
                  style={styles.ownerAvatar}
                  source={{uri: item.owner.avatar_url}}
                />
              </View>
            )}
          </View>
        )}
      </TouchableOpacity>
      <TouchableOpacity
        testID="expandIcon"
        onPress={_toggleExpansion}
        style={styles.iconContainer}>
        <Icon
          name={isExpanded ? 'expand-less' : 'expand-more'}
          size={24}
          color="black"
        />
      </TouchableOpacity>
    </Animated.View>
  );
};
