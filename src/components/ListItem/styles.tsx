import {StyleSheet} from 'react-native';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderWidth: 1,
    borderRadius: 8,
    marginVertical: 8,
    marginHorizontal: 24,
    backgroundColor: Colors.background,
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
    borderRadius: 8,
    padding: 16,
  },
  repoName: {
    fontSize: 20,
    lineHeight: 22,
    marginBottom: 8,
    color: Colors.black,
  },
  repoDescp: {
    fontSize: 16,
    lineHeight: 18,
    marginBottom: 8,
    color: Colors.black,
  },
  extraInfoContainer: {
    width: '100%',
    marginTop: 24,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  iconContainer: {
    margin: 16,
  },
  ownerAvatar: {
    width: 24,
    height: 24,
  },
  extraInfoItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 32,
  },
  extraInfoText: {
    fontSize: 14,
    lineHeight: 16,
    color: Colors.black,
  },
});
