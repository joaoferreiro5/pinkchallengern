import {Platform, StyleSheet} from 'react-native';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  container: {
    width: '100%',
    height: '97.5%',
    borderRadius: 16,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    backgroundColor: Colors.white,
  },
  textInput: {
    width: '85%',
    alignSelf: 'center',
    height: 40,
    margin: 8,
    borderRadius: 4,
    backgroundColor: Colors.background,
  },
  picker: {
    width: '85%',
    alignSelf: 'center',
    height: 40,
    borderRadius: 4,
    margin: 8,
    ...Platform.select({
      ios: {
        padding: 12,
      },
    }),
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightgray,
    backgroundColor: Colors.background,
  },
  pickerContent: {
    placeholder: {
      color: Colors.lightgray,
    },
    alignItems: 'center',
    justifyContent: 'center',
    color: Colors.black,
  },
  button: {
    alignSelf: 'center',
    marginTop: 12,
  },
  label: {
    fontSize: 16,
    lineHeight: 18,
    color: Colors.black,
    marginLeft: 24,
    marginTop: 24,
  },
});
