import React from 'react';
import {View, Text} from 'react-native';

import {TextInput, Button} from 'react-native-paper';
import RNPickerSelect from 'react-native-picker-select';

import Colors from '../../utils/colors';
import styles from './styles';

export interface Props {
  toggleSearch: () => void;
  languageSearchString: string;
  changeLanguageSearchText: (value: string) => void;
  topicSearchString: string;
  changeTopicSearchText: (value: string) => void;
  usernameSearchString: string;
  changeUsernameSearchText: (value: string) => void;
  changePicker: (value: string) => void;
}

export default ({
  toggleSearch,
  languageSearchString,
  changeLanguageSearchText,
  topicSearchString,
  changeTopicSearchText,
  usernameSearchString,
  changeUsernameSearchText,
  changePicker,
}: Props): JSX.Element => (
  <View style={styles.container}>
    <Text style={styles.label}>{'Language: '}</Text>
    <TextInput
      testID="languageTextInput"
      placeholderTextColor={Colors.lightgray}
      placeholder="Language"
      value={languageSearchString}
      style={styles.textInput}
      onChangeText={changeLanguageSearchText}
    />
    <Text style={styles.label}>{'Topic: '}</Text>
    <TextInput
      testID="topicTextInput"
      placeholderTextColor={Colors.lightgray}
      placeholder="Topic"
      value={topicSearchString}
      style={styles.textInput}
      onChangeText={changeTopicSearchText}
    />
    <Text style={styles.label}>{'Username: '}</Text>
    <TextInput
      testID="usernameTextInput"
      placeholderTextColor={Colors.lightgray}
      placeholder="Username"
      value={usernameSearchString}
      style={styles.textInput}
      onChangeText={changeUsernameSearchText}
    />
    <Text style={styles.label}>{'Sort by: '}</Text>
    <View style={styles.picker}>
      <RNPickerSelect
        style={styles.pickerContent}
        placeholder={{label: 'Sort by...', value: null}}
        items={[
          {label: 'Stars', value: 'stars'},
          {label: 'Forks', value: 'forks'},
          {label: 'Help Wanted Issues', value: 'help-wanted-issues'},
          {label: 'Last Updated', value: 'updated'},
        ]}
        onValueChange={changePicker}
      />
    </View>
    <Button
      testID="button"
      style={styles.button}
      mode="contained"
      onPress={(): void => {
        toggleSearch();
      }}>
      {'Apply Filters'}
    </Button>
  </View>
);
