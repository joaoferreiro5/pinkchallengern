import React from 'react';

import {Appbar, TextInput} from 'react-native-paper';

import styles from './styles';

interface Props {
  title: string;
  goBack?: () => void;
  addItemToStorage?: () => void;
  searchString?: string;
  changeSearchText?: (value: string) => void;
  toggleSearch?: () => void;
  toggleFilters?: () => void;
  offline?: boolean;
}

export default ({
  title,
  goBack,
  addItemToStorage,
  searchString,
  changeSearchText,
  toggleSearch,
  toggleFilters,
  offline,
}: Props): JSX.Element => {
  return goBack != null ? (
    <Appbar.Header style={styles.container}>
      <Appbar.BackAction onPress={goBack} />
      <Appbar.Content testID="title" title={title} />
      {addItemToStorage != null && (
        <Appbar.Action
          testID="saveIcon"
          icon="content-save"
          onPress={addItemToStorage}
        />
      )}
    </Appbar.Header>
  ) : (
    <Appbar.Header style={styles.container}>
      <Appbar.Content testID="title" title={title} />
      {!offline && (
        <TextInput
          testID="headerTextInput"
          placeholder="Search..."
          value={searchString}
          style={styles.textInput}
          onChangeText={changeSearchText}
          onEndEditing={toggleSearch}
        />
      )}
      {!offline && (
        <Appbar.Action
          testID="filterIcon"
          icon="filter"
          onPress={toggleFilters}
        />
      )}
    </Appbar.Header>
  );
};
