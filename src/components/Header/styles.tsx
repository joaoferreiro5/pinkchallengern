import {StyleSheet} from 'react-native';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  container: {
    top: 0,
    width: '100%',
    backgroundColor: Colors.lightblue,
  },
  textInput: {
    width: '40%',
    height: 40,
    borderRadius: 4,
  },
});
