import React, {ReactElement} from 'react';

import {
  SafeAreaView,
  ScrollView,
  View,
  KeyboardAvoidingView,
  Platform,
  ViewStyle,
  RefreshControlProps,
} from 'react-native';

import styles from './styles';

interface Props {
  header?: JSX.Element;
  children?: JSX.Element[] | JSX.Element;
  customStyles?: ViewStyle;
  backgroundColor?: string;
  scrollable?: boolean;
  refreshControl?: ReactElement<
    RefreshControlProps,
    string | React.JSXElementConstructor<unknown>
  >;
}

export default ({
  header,
  children,
  customStyles,
  backgroundColor,
  scrollable,
  refreshControl,
}: Props): JSX.Element => {
  return (
    <KeyboardAvoidingView
      style={styles.keyboardAvoidingView}
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      {scrollable ? (
        <SafeAreaView
          style={[
            styles.flexOne,
            backgroundColor != null && {
              backgroundColor: backgroundColor,
            },
          ]}>
          <ScrollView
            refreshControl={refreshControl}
            contentContainerStyle={[
              styles.flexOne,
              backgroundColor != null && {
                backgroundColor: backgroundColor,
              },
            ]}>
            <View
              style={[
                styles.container,
                customStyles,
                backgroundColor != null && {
                  backgroundColor: backgroundColor,
                },
              ]}>
              {header}
              {children}
            </View>
          </ScrollView>
        </SafeAreaView>
      ) : (
        <SafeAreaView
          style={[
            styles.container,
            customStyles,
            backgroundColor != null && {
              backgroundColor: backgroundColor,
            },
          ]}>
          {header}
          {children}
        </SafeAreaView>
      )}
    </KeyboardAvoidingView>
  );
};
