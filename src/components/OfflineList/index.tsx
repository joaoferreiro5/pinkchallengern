import React from 'react';
import {View, Text} from 'react-native';

import {NavigationProp, ParamListBase} from '@react-navigation/native';

import {useStorageState} from '../../contexts/StorageContext';
import {RepoInterface} from '../../contexts/types';

import ListItem from '../ListItem';

import styles from './styles';

interface Props {
  navigation: NavigationProp<ParamListBase>;
}

export default ({navigation}: Props): JSX.Element => {
  const storageState = useStorageState();

  return (
    <>
      <View style={styles.container}>
        {storageState.values.repos != null &&
        storageState.values.repos.length !== 0 ? (
          storageState.values.repos.map(
            (item: RepoInterface, index: number) => (
              <ListItem
                navigation={navigation}
                item={item}
                offline
                key={`list-item-${index}`}
              />
            ),
          )
        ) : (
          <Text style={styles.emptySearchText}>
            {"You haven't saved any repositories"}
          </Text>
        )}
      </View>
    </>
  );
};
