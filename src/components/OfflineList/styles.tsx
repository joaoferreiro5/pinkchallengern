import {StyleSheet} from 'react-native';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    marginVertical: 24,
    width: '100%',
  },
  emptySearchText: {
    marginTop: '50%',
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 24,
    lineHeight: 32,
    color: Colors.lightgray,
  },
});
