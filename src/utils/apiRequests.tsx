import axios, {AxiosError, AxiosResponse} from 'axios';

type Methods = 'get' | 'delete' | 'head' | 'options' | 'post' | 'put' | 'patch';

export function getRepositories(params: {
  q?: string;
  sort?: string;
  order?: string;
  per_page?: number;
  page?: number;
}): Promise<AxiosResponse> {
  return apiRequest('get', '/search/repositories', params);
}

function apiRequest(
  requestType: Methods,
  url: string,
  params?: any,
): Promise<AxiosResponse> {
  return new Promise<AxiosResponse>((resolve, reject) => {
    axios[requestType](url, requestType === 'get' ? {params: params} : params)
      .then((response: AxiosResponse) => resolve(response))
      .catch(function (error: AxiosError) {
        if (!error.response) {
          reject(error);
        } else {
          reject(error.response);
        }
      });
  });
}
