export default {
  black: '#000',
  white: '#fff',
  gray: '#999',
  background: '#eee',
  darkgray: '#263238',
  lightgray: '#c9c9c9',
  red: '#dd2c00',
  blue: '#01579b',
  green: '#00c853',
  lightblue: '#b3e5fc',
};
