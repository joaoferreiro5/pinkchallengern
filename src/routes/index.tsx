import React, {useEffect} from 'react';
import {Platform, Text, StyleSheet} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import axios from 'axios';
import Config from 'react-native-config';

import Home from '../screens/Home';
import Offline from '../screens/Offline';
import Repository from '../screens/Repository';

import Icon from 'react-native-vector-icons/MaterialIcons';

import Colors from '../utils/colors';

export default (): JSX.Element => {
  const MainStack = createNativeStackNavigator();
  const TabStack = createBottomTabNavigator();

  useEffect(() => {
    axios.defaults.baseURL = Config.API_URL;
  }, []);

  const BottomTabs = (): JSX.Element => (
    <TabStack.Navigator
      screenOptions={{
        headerShown: false,
        tabBarLabelStyle: {
          fontFamily: 'Poppins-Regular',
          fontSize: 13,
          lineHeight: 14,
          marginBottom: 16,
        },
        tabBarStyle: {
          ...Platform.select({
            ios: {
              height: 100,
            },
            android: {
              height: 75,
            },
          }),
        },
        tabBarActiveTintColor: Colors.darkgray,
        tabBarInactiveTintColor: Colors.lightgray,
      }}>
      <TabStack.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({color}): JSX.Element => (
            <Icon name="home" size={20} color={color} />
          ),
          tabBarLabel: ({color, focused}): JSX.Element => (
            <Text
              style={[
                focused ? styles.activeLabel : styles.inactiveLabel,
                {color: color},
              ]}>
              {'Home'}
            </Text>
          ),
        }}
      />
      <TabStack.Screen
        name="Offline"
        component={Offline}
        options={{
          tabBarIcon: ({color}): JSX.Element => (
            <Icon name="wifi-off" size={20} color={color} />
          ),
          tabBarLabel: ({color, focused}): JSX.Element => (
            <Text
              style={[
                focused ? styles.activeLabel : styles.inactiveLabel,
                {color: color},
              ]}>
              {'Offline'}
            </Text>
          ),
        }}
      />
    </TabStack.Navigator>
  );

  return (
    <NavigationContainer>
      <MainStack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <MainStack.Screen name="BottomTabs" component={BottomTabs} />
        <MainStack.Screen name="Repository" component={Repository} />
      </MainStack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  activeLabel: {
    fontWeight: '700',
    fontSize: 13,
    lineHeight: 14,
    marginBottom: 16,
  },
  inactiveLabel: {
    fontWeight: 'normal',
    fontSize: 13,
    lineHeight: 14,
    marginBottom: 16,
  },
});
