import * as React from 'react';
import {RepoInterface} from '../types';

export interface ApiContextInterface {
  values: {
    refreshing: boolean;
    repos: RepoInterface[] | null;
    totalRepoItems: number | null;
  };
  actions: {
    fetchRepos: (params: {
      q: string;
      sort: string;
      order: string;
      per_page: number;
      page: number;
    }) => void;
  };
}

export const ApiContext = React.createContext<ApiContextInterface | null>(null);

export const ApiProvider = ApiContext.Provider;

export const ApiConsumer = ApiContext.Consumer;
