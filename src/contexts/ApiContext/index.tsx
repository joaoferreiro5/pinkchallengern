import React, {useContext, useState} from 'react';
import {Alert} from 'react-native';

import {
  ApiProvider,
  ApiConsumer,
  ApiContext,
  ApiContextInterface,
} from './context';

import {RepoInterface} from '../types';

import {getRepositories} from '../../utils/apiRequests';

function ApiContextProvider({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}): JSX.Element {
  const [refreshing, setRefreshing] = useState<boolean>(false);
  const [repos, setRepos] = useState<RepoInterface[] | null>(null);
  const [totalRepoItems, setTotalRepoItems] = useState<number | null>(null);

  const fetchRepos = ({
    q,
    sort,
    order,
    per_page,
    page,
  }: {
    q: string;
    sort: string;
    order: string;
    per_page: number;
    page: number;
  }): void => {
    setRefreshing(true);
    getRepositories({q, sort, order, per_page, page})
      .then(response => {
        setTotalRepoItems(response.data.total_count);
        setRepos(response.data.items);
      })
      .catch(e => {
        if (e.status === 422) {
          setRepos([]);
        } else if (e.status === 403) {
          Alert.alert(
            'Request limit exceeded. Please wait a few minutes before retrying...',
          );
        } else if (!e.status) {
          console.log('There was a network error.');
        } else {
          Alert.alert('An unkonw error as occured. Please try again.');
          setRepos(null);
          console.log(e);
        }
      })
      .finally(() => setRefreshing(false));
  };

  return (
    <ApiProvider
      value={{
        values: {
          totalRepoItems,
          refreshing,
          repos,
        },
        actions: {
          fetchRepos,
        },
      }}>
      {children}
    </ApiProvider>
  );
}

function useApiState(): ApiContextInterface {
  const context = useContext(ApiContext);
  if (context === null) {
    throw new Error('useApiState must be used within an ApiContextProvider');
  }
  return context;
}

export {ApiContextProvider, ApiConsumer, useApiState};
