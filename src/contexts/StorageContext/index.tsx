import React, {useContext, useState} from 'react';
import {Alert} from 'react-native';

import {
  StorageProvider,
  StorageConsumer,
  StorageContext,
  StorageContextInterface,
} from './context';

import {useApiState} from '../ApiContext';

import {RepoInterface} from '../types';

import AsyncStorage from '@react-native-async-storage/async-storage';

function StorageContextProvider({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}): JSX.Element {
  const apiState = useApiState();
  const [loading, setLoading] = useState<boolean>(false);
  const [repos, setRepos] = useState<RepoInterface[] | null>(null);

  const getRepos = async (): Promise<void> => {
    setLoading(true);
    try {
      const keys = await AsyncStorage.getAllKeys();
      keys.filter((item: string) => item.includes('prchallengeapp-'));
      const rawRepos = await AsyncStorage.multiGet(keys as string[]);
      setRepos((rawRepos ?? []).map((item: any) => JSON.parse(item[1])));
    } catch (e) {
      console.log(e);
    }
    setLoading(false);
  };

  const addRepo = async (id: number): Promise<void> => {
    setLoading(true);
    const repo = apiState.values.repos?.find(
      (item: RepoInterface) => item.id === id,
    );
    if (repo != null) {
      try {
        await AsyncStorage.setItem(
          `prchallengeapp-${id}`,
          JSON.stringify(repo),
        );
      } catch (e) {
        console.log(e);
        Alert.alert('Error saving Repository. Please try again.');
      }
    }
    setLoading(false);
  };

  const removeRepo = async (id: number): Promise<void> => {
    setLoading(true);
    try {
      await AsyncStorage.removeItem(`prchallengeapp-${id}`);
    } catch (e) {
      console.log(e);
      Alert.alert('Error removing Repository. Please try again.');
    }
    setLoading(false);
  };

  const doesItemExist = async (id: number): Promise<boolean> => {
    setLoading(true);
    try {
      const repo = await AsyncStorage.getItem(`prchallengeapp-${id}`);
      if (repo != null) {
        return true;
      }
    } catch (e) {
      console.log(e);
      Alert.alert('Error removing Repository. Please try again.');
    }
    setLoading(false);
    return false;
  };

  return (
    <StorageProvider
      value={{
        values: {
          repos,
          loading,
        },
        actions: {
          getRepos,
          addRepo,
          removeRepo,
          doesItemExist,
        },
      }}>
      {children}
    </StorageProvider>
  );
}

function useStorageState(): StorageContextInterface {
  const context = useContext(StorageContext);
  if (context === null) {
    throw new Error(
      'useStorageState must be used within an StorageContextProvider',
    );
  }
  return context;
}

export {StorageContextProvider, StorageConsumer, useStorageState};
