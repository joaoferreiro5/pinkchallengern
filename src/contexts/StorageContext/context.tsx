import * as React from 'react';
import {RepoInterface} from '../types';

export interface StorageContextInterface {
  values: {
    loading: boolean;
    repos: RepoInterface[] | null;
  };
  actions: {
    getRepos: () => Promise<void>;
    addRepo: (id: number) => Promise<void>;
    removeRepo: (id: number) => Promise<void>;
    doesItemExist: (id: number) => Promise<boolean>;
  };
}

export const StorageContext =
  React.createContext<StorageContextInterface | null>(null);

export const StorageProvider = StorageContext.Provider;

export const StorageConsumer = StorageContext.Consumer;
