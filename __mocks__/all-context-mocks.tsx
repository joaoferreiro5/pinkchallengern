import React from 'react';

import {ApiProvider} from '../src/contexts/ApiContext/context';
import {StorageProvider} from '../src/contexts/StorageContext/context';
import {
  route,
  fetchReposMock,
  getReposMock,
  removeRepoMock,
  addRepoMock,
  doesItemExistMock,
} from './utilsMock';

const ContextsMock = (
  {component}: {component: JSX.Element},
  apiValue = {
    values: {refreshing: false, repos: [route.params.item], totalRepoItems: 1},
    actions: {fetchRepos: fetchReposMock},
  },
  storageValue = {
    values: {loading: false, repos: [route.params.item]},
    actions: {
      getRepos: getReposMock,
      addRepo: addRepoMock,
      removeRepo: removeRepoMock,
      doesItemExist: doesItemExistMock,
    },
  },
) => (
  <ApiProvider value={apiValue}>
    <StorageProvider value={storageValue}>{component}</StorageProvider>
  </ApiProvider>
);

export default ContextsMock;
