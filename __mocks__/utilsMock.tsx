export const navigation = {
  navigate: jest.fn(),
  dispatch: jest.fn(),
  goBack: jest.fn(),
  reset: jest.fn(),
  isFocused: jest.fn(),
  canGoBack: jest.fn(),
  getParent: jest.fn(),
  getState: jest.fn(),
  setParams: jest.fn(),
  setOptions: jest.fn(),
  addListener: jest.fn(),
  removeListener: jest.fn(),
};

export const route = {
  params: {
    item: {
      allow_forking: true,
      archive_url:
        'https://api.github.com/repos/storybookjs/storybook/{archive_format}{/ref}',
      archived: false,
      assignees_url:
        'https://api.github.com/repos/storybookjs/storybook/assignees{/user}',
      blobs_url:
        'https://api.github.com/repos/storybookjs/storybook/git/blobs{/sha}',
      branches_url:
        'https://api.github.com/repos/storybookjs/storybook/branches{/branch}',
      clone_url: 'https://github.com/storybookjs/storybook.git',
      collaborators_url:
        'https://api.github.com/repos/storybookjs/storybook/collaborators{/collaborator}',
      comments_url:
        'https://api.github.com/repos/storybookjs/storybook/comments{/number}',
      commits_url:
        'https://api.github.com/repos/storybookjs/storybook/commits{/sha}',
      compare_url:
        'https://api.github.com/repos/storybookjs/storybook/compare/{base}...{head}',
      contents_url:
        'https://api.github.com/repos/storybookjs/storybook/contents/{+path}',
      contributors_url:
        'https://api.github.com/repos/storybookjs/storybook/contributors',
      created_at: '2016-03-18T04:23:44Z',
      default_branch: 'next',
      deployments_url:
        'https://api.github.com/repos/storybookjs/storybook/deployments',
      description:
        '📓 The UI component explorer. Develop, document, & test React, Vue, Angular, Web Components, Ember, Svelte & more!',
      disabled: false,
      downloads_url:
        'https://api.github.com/repos/storybookjs/storybook/downloads',
      events_url: 'https://api.github.com/repos/storybookjs/storybook/events',
      fork: false,
      forks: 7097,
      forks_count: 7097,
      forks_url: 'https://api.github.com/repos/storybookjs/storybook/forks',
      full_name: 'storybookjs/storybook',
      git_commits_url:
        'https://api.github.com/repos/storybookjs/storybook/git/commits{/sha}',
      git_refs_url:
        'https://api.github.com/repos/storybookjs/storybook/git/refs{/sha}',
      git_tags_url:
        'https://api.github.com/repos/storybookjs/storybook/git/tags{/sha}',
      git_url: 'git://github.com/storybookjs/storybook.git',
      has_downloads: true,
      has_issues: true,
      has_pages: false,
      has_projects: true,
      has_wiki: false,
      homepage: 'https://storybook.js.org',
      hooks_url: 'https://api.github.com/repos/storybookjs/storybook/hooks',
      html_url: 'https://github.com/storybookjs/storybook',
      id: 54173593,
      is_template: false,
      issue_comment_url:
        'https://api.github.com/repos/storybookjs/storybook/issues/comments{/number}',
      issue_events_url:
        'https://api.github.com/repos/storybookjs/storybook/issues/events{/number}',
      issues_url:
        'https://api.github.com/repos/storybookjs/storybook/issues{/number}',
      keys_url:
        'https://api.github.com/repos/storybookjs/storybook/keys{/key_id}',
      labels_url:
        'https://api.github.com/repos/storybookjs/storybook/labels{/name}',
      language: 'TypeScript',
      languages_url:
        'https://api.github.com/repos/storybookjs/storybook/languages',
      license: {
        key: 'mit',
        name: 'MIT License',
        node_id: 'MDc6TGljZW5zZTEz',
        spdx_id: 'MIT',
        url: 'https://api.github.com/licenses/mit',
        html_url: '',
      },
      merges_url: 'https://api.github.com/repos/storybookjs/storybook/merges',
      milestones_url:
        'https://api.github.com/repos/storybookjs/storybook/milestones{/number}',
      mirror_url: '',
      name: 'storybook',
      node_id: 'MDEwOlJlcG9zaXRvcnk1NDE3MzU5Mw==',
      notifications_url:
        'https://api.github.com/repos/storybookjs/storybook/notifications{?since,all,participating}',
      open_issues: 1669,
      open_issues_count: 1669,
      owner: {
        avatar_url: 'https://avatars.githubusercontent.com/u/22632046?v=4',
        events_url: 'https://api.github.com/users/storybookjs/events{/privacy}',
        followers_url: 'https://api.github.com/users/storybookjs/followers',
        following_url:
          'https://api.github.com/users/storybookjs/following{/other_user}',
        gists_url: 'https://api.github.com/users/storybookjs/gists{/gist_id}',
        gravatar_id: '',
        html_url: 'https://github.com/storybookjs',
        id: 22632046,
        login: 'storybookjs',
        node_id: 'MDEyOk9yZ2FuaXphdGlvbjIyNjMyMDQ2',
        organizations_url: 'https://api.github.com/users/storybookjs/orgs',
        received_events_url:
          'https://api.github.com/users/storybookjs/received_events',
        repos_url: 'https://api.github.com/users/storybookjs/repos',
        site_admin: false,
        starred_url:
          'https://api.github.com/users/storybookjs/starred{/owner}{/repo}',
        subscriptions_url:
          'https://api.github.com/users/storybookjs/subscriptions',
        type: 'Organization',
        url: 'https://api.github.com/users/storybookjs',
      },
      private: false,
      pulls_url:
        'https://api.github.com/repos/storybookjs/storybook/pulls{/number}',
      pushed_at: '2022-02-14T15:18:57Z',
      releases_url:
        'https://api.github.com/repos/storybookjs/storybook/releases{/id}',
      score: 1,
      size: 471561,
      ssh_url: 'git@github.com:storybookjs/storybook.git',
      stargazers_count: 68741,
      stargazers_url:
        'https://api.github.com/repos/storybookjs/storybook/stargazers',
      statuses_url:
        'https://api.github.com/repos/storybookjs/storybook/statuses/{sha}',
      subscribers_url:
        'https://api.github.com/repos/storybookjs/storybook/subscribers',
      subscription_url:
        'https://api.github.com/repos/storybookjs/storybook/subscription',
      svn_url: 'https://github.com/storybookjs/storybook',
      tags_url: 'https://api.github.com/repos/storybookjs/storybook/tags',
      teams_url: 'https://api.github.com/repos/storybookjs/storybook/teams',
      topics: [
        'angular',
        'components',
        'design-systems',
        'documentation',
        'ember',
        'html',
        'javascript',
        'polymer',
        'react',
        'react-native',
        'storybook',
        'styleguide',
        'svelte',
        'testing',
        'typescript',
        'ui',
        'ui-components',
        'vue',
        'web-components',
        'webpack',
      ],
      trees_url:
        'https://api.github.com/repos/storybookjs/storybook/git/trees{/sha}',
      updated_at: '2022-02-14T15:23:06Z',
      url: 'https://api.github.com/repos/storybookjs/storybook',
      visibility: 'public',
      watchers: 68741,
      watchers_count: 68741,
      master_branch: '',
    },
  },
};

export const changeUsernameSearchTextMock = jest.fn();
export const toggleSearchMock = jest.fn();
export const changePickerMock = jest.fn();
export const changeLanguageSearchTextMock = jest.fn();
export const changeTopicSearchTextMock = jest.fn();
export const fetchReposMock = jest.fn();
export const getReposMock = jest.fn();
export const addRepoMock = jest.fn();
export const removeRepoMock = jest.fn();
export const doesItemExistMock = jest
  .fn()
  .mockResolvedValueOnce(true)
  .mockResolvedValue(false);
