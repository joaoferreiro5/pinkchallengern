const mockedModule = jest.mock('@react-navigation/native-stack');

const createBottomTabNavigator = jest.fn().mockReturnValue({
  Stack: jest.fn(),
});

export {createBottomTabNavigator};
export default mockedModule;
