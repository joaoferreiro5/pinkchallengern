const mockedModule = jest.mock('@react-navigation/native-stack');

const createNativeStackNavigator = jest.fn().mockReturnValue({
  Stack: jest.fn(),
});

export {createNativeStackNavigator};
export default mockedModule;
