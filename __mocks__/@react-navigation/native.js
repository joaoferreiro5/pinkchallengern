const mockedModule = jest.mock('@react-navigation/native');

// const useNavigation = jest.fn().mockReturnValue({navigate: x => x});
const useIsFocused = jest.fn(() => true);

export {useIsFocused};
export default mockedModule;
