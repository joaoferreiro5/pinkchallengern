import React from 'react';

import renderer from 'react-test-renderer';
import {cleanup, render, fireEvent} from '@testing-library/react-native';

import Header from '../../src/components/Header';

describe('Testing Header component', () => {
  afterEach(cleanup);

  const changeSearchText = jest.fn();
  const toggleSearch = jest.fn();
  const toggleFilters = jest.fn();
  const goBack = jest.fn();
  const addItemToStorage = jest.fn();

  const headerOffline = <Header title={'Saved Repos'} offline />;
  const headerHome = (
    <Header
      title={'GitHub Repos'}
      searchString={''}
      changeSearchText={changeSearchText}
      toggleSearch={toggleSearch}
      toggleFilters={toggleFilters}
    />
  );
  const headerRepo = (
    <Header
      title={'Repo Name'}
      goBack={goBack}
      addItemToStorage={addItemToStorage}
    />
  );

  test('matches snapshot & render correctly', () => {
    const firstRendered = renderer.create(headerOffline).toJSON();
    const secondRendered = renderer.create(headerHome).toJSON();
    const thirdRendered = renderer.create(headerRepo).toJSON();
    expect(secondRendered).toMatchSnapshot();
    expect(secondRendered).toBeTruthy();
    expect(firstRendered).toMatchSnapshot();
    expect(firstRendered).toBeTruthy();
    expect(thirdRendered).toMatchSnapshot();
    expect(thirdRendered).toBeTruthy();
  });

  test('changes text inputs', () => {
    const {getAllByTestId} = render(headerHome);
    const languageTextInput = getAllByTestId('headerTextInput');

    fireEvent.changeText(languageTextInput[0], 'repoTest');
    expect(changeSearchText).toHaveBeenCalledWith('repoTest');

    fireEvent(languageTextInput[0], 'endEditing');
    expect(toggleSearch).toHaveBeenCalled();
  });

  test('presses filters button', () => {
    const {getAllByTestId} = render(headerHome);
    const languageTextInput = getAllByTestId('filterIcon');

    fireEvent.press(languageTextInput[0]);
    expect(toggleFilters).toHaveBeenCalled();
  });

  test('presses save button', () => {
    const {getAllByTestId} = render(headerRepo);
    const languageTextInput = getAllByTestId('saveIcon');

    fireEvent.press(languageTextInput[0]);
    expect(addItemToStorage).toHaveBeenCalled();
  });
});
