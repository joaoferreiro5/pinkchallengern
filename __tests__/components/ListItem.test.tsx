import React from 'react';
import ListItem from '../../src/components/ListItem';

import ContextsMock from '../../__mocks__/all-context-mocks';

import {render, fireEvent} from '@testing-library/react-native';
import renderer from 'react-test-renderer';

import {navigation, route} from '../../__mocks__/utilsMock';

describe('Testing ListItem component', () => {
  function componentWithContext(): JSX.Element {
    return ContextsMock({
      component: <ListItem navigation={navigation} item={route.params.item} />,
    });
  }

  test('matches snapshot & render correctly', () => {
    const rendered = renderer.create(componentWithContext()).toJSON();
    expect(rendered).toMatchSnapshot();
    expect(rendered).toBeTruthy();
  });

  test('it expands item', () => {
    const {getAllByTestId, queryByTestId} = render(componentWithContext());
    const expandIcon = getAllByTestId('expandIcon');
    const extraInfoQuery = queryByTestId('extraInfo');
    expect(extraInfoQuery).toBe(null);
    fireEvent.press(expandIcon[0]);
    const extraInfo = getAllByTestId('extraInfo');
    expect(extraInfo).not.toBe(null);
  });

  test('it opens item', () => {
    const {getAllByTestId} = render(componentWithContext());
    const button = getAllByTestId('navigateIcon');
    fireEvent.press(button[0]);
    expect(navigation.navigate).toHaveBeenCalled();
  });
});

// describe('tests with enzyme', () => {
//   let wrapper: ReactWrapper<JSX.Element, Props, {}>;
//   beforeAll(() => {
//     wrapper = mount(componentWithContext());
//   });
//   it('expands item onClick', () => {
//     expect(
//       wrapper.findWhere(node => node.prop('testID') === 'extraInfo'),
//     ).toHaveLength(0);

//     wrapper
//       .findWhere(node => node.prop('testID') === 'expandIcon')
//       .first()
//       .simulate('click');

//     expect(
//       wrapper.findWhere(node => node.prop('testID') === 'extraInfo'),
//     ).not.toHaveLength(0);
//   });

//   it('navigates on click', () => {
//     const button = wrapper
//       .findWhere(node => node.prop('testID') === 'navigateIcon')
//       .first();
//     expect(button).toBeTruthy();
//     button.simulate('click');

//     expect(navigation.navigate).toHaveBeenCalled();
//   });
// });
