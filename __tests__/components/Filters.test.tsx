import React from 'react';

import renderer from 'react-test-renderer';
import {cleanup, render, fireEvent} from '@testing-library/react-native';

import ContextsMock from '../../__mocks__/all-context-mocks';
import Filters from '../../src/components/Filters';

import {
  changeUsernameSearchTextMock,
  changeTopicSearchTextMock,
  changeLanguageSearchTextMock,
  changePickerMock,
  toggleSearchMock,
} from '../../__mocks__/utilsMock';

describe('Testing Filters component', () => {
  afterEach(cleanup);

  function componentWithContext(): JSX.Element {
    return ContextsMock({
      component: (
        <Filters
          languageSearchString={''}
          topicSearchString={''}
          usernameSearchString={''}
          changeUsernameSearchText={changeUsernameSearchTextMock}
          toggleSearch={toggleSearchMock}
          changePicker={changePickerMock}
          changeLanguageSearchText={changeLanguageSearchTextMock}
          changeTopicSearchText={changeTopicSearchTextMock}
        />
      ),
    });
  }

  test('matches snapshot & render correctlyT', () => {
    const rendered = renderer.create(componentWithContext()).toJSON();
    expect(rendered).toMatchSnapshot();
    expect(rendered).toBeTruthy();
  });

  test('changes text inputs', () => {
    const {getAllByTestId} = render(componentWithContext());
    const languageTextInput = getAllByTestId('languageTextInput');
    fireEvent.changeText(languageTextInput[0], 'languageTest');
    expect(changeLanguageSearchTextMock).toHaveBeenCalledWith('languageTest');

    const topicTextInput = getAllByTestId('topicTextInput');
    fireEvent.changeText(topicTextInput[0], 'topicTest');
    expect(changeTopicSearchTextMock).toHaveBeenCalledWith('topicTest');

    const usernameTextInput = getAllByTestId('usernameTextInput');
    fireEvent.changeText(usernameTextInput[0], 'usernameTest');
    expect(changeUsernameSearchTextMock).toHaveBeenCalledWith('usernameTest');
  });

  test('presses button', () => {
    const {getAllByTestId} = render(componentWithContext());
    const button = getAllByTestId('button');
    fireEvent.press(button[0]);
    expect(toggleSearchMock).toHaveBeenCalled();
  });
});
