import React from 'react';
import {Linking} from 'react-native';
import Repository from '../../src/screens/Repository';

import ContextsMock from '../../__mocks__/all-context-mocks';

import renderer from 'react-test-renderer';
import {
  cleanup,
  render,
  fireEvent,
  waitFor,
} from '@testing-library/react-native';

import {
  navigation,
  doesItemExistMock,
  route,
  removeRepoMock,
  addRepoMock,
} from '../../__mocks__/utilsMock';

describe('Testing Repository component', () => {
  afterEach(cleanup);

  function componentWithContext(): JSX.Element {
    return ContextsMock({
      component: <Repository navigation={navigation} route={route} />,
    });
  }

  test('matches snapshot & render correctly', () => {
    const rendered = renderer.create(componentWithContext()).toJSON();
    expect(rendered).toMatchSnapshot();
    expect(rendered).toBeTruthy();
  });

  describe('Header tests', () => {
    const {container} = render(componentWithContext());
    const wrapper = container.children[0];
    //@ts-ignore
    const header = wrapper.props.header;
    renderer.act(() => {
      header.props.goBack();
    });
    waitFor(() => expect(navigation.goBack).toHaveBeenCalledWith());
  });

  describe('Component tests', () => {
    test("clicks url's", () => {
      const canOpenURL = jest
        .spyOn(Linking, 'canOpenURL')
        .mockImplementation(() => Promise.resolve(true));

      const {getAllByTestId} = render(componentWithContext());
      const urls = getAllByTestId('url');
      urls.forEach(item => {
        fireEvent.press(item);
        expect(canOpenURL).toHaveBeenCalled();
      });
    });

    test('test', () => {
      const myInitialState = true;
      React.useState = jest.fn().mockReturnValue([myInitialState, {}]);

      const {getAllByTestId} = render(componentWithContext());
      expect(doesItemExistMock).toHaveBeenCalledWith(54173593);
      jest.runAllTimers();

      const deleteButton = getAllByTestId('deleteButton');
      fireEvent.press(deleteButton[0]);
      expect(removeRepoMock).toHaveBeenCalledWith(54173593);
      expect(doesItemExistMock).toHaveBeenCalledWith(54173593);
    });

    test('test2', () => {
      const myInitialState = false;
      React.useState = jest.fn().mockReturnValue([myInitialState, {}]);

      const {container} = render(componentWithContext());
      const wrapper = container.children[0];
      //@ts-ignore
      const header = wrapper.props.header;
      renderer.act(() => {
        header.props.addItemToStorage();
      });
      waitFor(() => expect(addRepoMock).toHaveBeenCalled());
    });
  });
});
