import React from 'react';
import Home from '../../src/screens/Home';

import ContextsMock from '../../__mocks__/all-context-mocks';

import {cleanup, render, waitFor} from '@testing-library/react-native';
import renderer from 'react-test-renderer';

import {navigation} from '../../__mocks__/utilsMock';

describe('Testing Home component', () => {
  afterEach(cleanup);

  function componentWithContext(): JSX.Element {
    return ContextsMock({
      component: <Home navigation={navigation} />,
    });
  }

  test('matches snapshot & render correctly', () => {
    const rendered = renderer.create(componentWithContext()).toJSON();
    expect(rendered).toMatchSnapshot();
    expect(rendered).toBeTruthy();
  });

  test('run timer', () => {
    render(componentWithContext());
    jest.advanceTimersByTime(70000);
  });

  describe('Header tests', () => {
    test('toggle filters', () => {
      const {container} = render(componentWithContext());
      const wrapper = container.children[0];
      //@ts-ignore
      const header = wrapper.props.header;
      //@ts-ignore
      const modal = wrapper.props.children[1];
      expect(modal.props.visible).toBe(false);
      renderer.act(() => {
        header.props.toggleFilters();
      });
      waitFor(() => expect(modal.props.visible).toBe(true));
      renderer.act(() => {
        modal.props.onDismiss();
      });
      waitFor(() => {
        expect(modal.props.visible).toBe(false);
      });
    });

    test('change search Text', () => {
      const {container} = render(componentWithContext());
      const wrapper = container.children[0];
      //@ts-ignore
      const header = wrapper.props.header;
      expect(header.props.searchString).toBe('');
      renderer.act(() => {
        header.props.changeSearchText('test');
      });
      waitFor(() => expect(header.props.searchString).toBe('test'));
    });

    test('toggle search', () => {
      const {container} = render(componentWithContext());
      const wrapper = container.children[0];
      //@ts-ignore
      const header = wrapper.props.header;
      //@ts-ignore
      const modal = wrapper.props.children[1];
      expect(modal.props.visible).toBe(false);
      renderer.act(() => {
        header.props.toggleSearch();
      });
      waitFor(() => {
        expect(modal.props.visible).toBe(true);
      });
    });
  });

  describe('List tests', () => {
    test('change page', () => {
      const {container} = render(componentWithContext());
      const wrapper = container.children[0];
      //@ts-ignore
      const list = wrapper.props.children[0];
      expect(list.props.currentPage).toBe(1);
      renderer.act(() => {
        list.props.increasePage();
      });
      waitFor(() => {
        expect(list.props.currentPage).toBe(2);
      });
      renderer.act(async () => {
        list.props.decreasePage();
      });
      waitFor(() => {
        expect(list.props.currentPage).toBe(1);
      });
      // limits at 1
      renderer.act(() => {
        list.props.decreasePage();
      });
      waitFor(() => {
        expect(list.props.currentPage).toBe(1);
      });
    });
  });

  describe('Modal tests', () => {
    test('change inputs', () => {
      const {container} = render(componentWithContext());
      const wrapper = container.children[0];
      //@ts-ignore
      const filters = wrapper.props.children[1].props.children;

      expect(filters.props.languageSearchString).toBe('');
      renderer.act(() => {
        filters.props.changeLanguageSearchText('test');
      });
      waitFor(() => {
        expect(filters.props.languageSearchString).toBe('test');
      });

      expect(filters.props.topicSearchString).toBe('');
      renderer.act(() => {
        filters.props.changeTopicSearchText('test');
      });
      waitFor(() => {
        expect(filters.props.topicSearchString).toBe('test');
      });

      expect(filters.props.usernameSearchString).toBe('');
      renderer.act(() => {
        filters.props.changeUsernameSearchText('test');
      });
      waitFor(() => {
        expect(filters.props.usernameSearchString).toBe('test');
      });
    });

    test('change picker', () => {
      const {container} = render(componentWithContext());
      const wrapper = container.children[0];
      //@ts-ignore
      const filters = wrapper.props.children[1].props.children;
      renderer.act(() => {
        filters.props.changePicker('value');
      });
      // waitFor(() => {
      //   expect(filters.props.).toBe();
      // });
    });
  });
});
