import React from 'react';
import Offline from '../../src/screens/Offline';

import ContextsMock from '../../__mocks__/all-context-mocks';

import renderer from 'react-test-renderer';
import {cleanup, render} from '@testing-library/react-native';

import {navigation} from '../../__mocks__/utilsMock';

describe('Testing Offline component', () => {
  afterEach(cleanup);

  function componentWithContext(): JSX.Element {
    return ContextsMock({
      component: <Offline navigation={navigation} />,
    });
  }

  test('matches snapshot & render correctly', () => {
    const rendered = renderer.create(componentWithContext()).toJSON();
    expect(rendered).toMatchSnapshot();
    expect(rendered).toBeTruthy();
  });

  test('test useEffect', () => {
    render(componentWithContext());
  });
});
