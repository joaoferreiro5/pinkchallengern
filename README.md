# Pink Room Technical Challenge

## Challenge

Adaptation of the [Android Challenge](https://gitlab.com/pinkroom/android-challenge) for React Native.

## Getting started

1 - Clone the repo `git clone git@gitlab.com:joaoferreiro5/pinkchallengern.git`.

2 - Make sure you have the prerequisites installed (e.g. node, react-native, android studio, xcode), verify [here](https://facebook.github.io/react-native/docs/getting-started).

3 - Install the required node modules `yarn install`.

4 - (iOS only) Install the cocoapods required for iOS `cd ios && pod install`

## Development

Start the application by executing one of the available scripts:
 - `yarn android:debug`
 - `yarn android:release`
 - `yarn ios:debug`
 - `yarn ios:release`

## Building the application

### Android
 - Create an keystore using the following command `keytool -genkey -v -keystore release.keystore -alias release -keyalg RSA -keysize 2048 -validity 10000`
Note that the keystore alias, passwords and name must match the ones found on `android/app/build.gradle`
 - Execute one of the following available scripts:
	1. `yarn android:build` to genereate an .apk file
	2. `yarn android:bundle` to generate an .aab file
 - (Optional) Install the apk using the following command `adb install -r ./app/build/outputs/apk/release/app-release.apk`

### iOS
To build the application for iOS you must use XCode.
 - Open the following file `ios/PinkChallengeRN.xcworkspace`
 - Select the `PinkChallengeRNRelease` scheme
 - Select `Any iOS Device` as target
 - Product > Archive and follow the instructions