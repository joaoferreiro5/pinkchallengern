/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';

import {ApiContextProvider} from './src/contexts/ApiContext';
import {StorageContextProvider} from './src/contexts/StorageContext';

import Routes from './src/routes';

const App = () => {
  return (
    <ApiContextProvider>
      <StorageContextProvider>
        <Routes />
      </StorageContextProvider>
    </ApiContextProvider>
  );
};

export default App;
