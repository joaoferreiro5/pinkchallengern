declare module '*.svg' {
  const content: React.FC<SvgProps>;
  export default content;
}
